#!/usr/bin/env bash




# ============================================================ #
# ================== < Load Configurables > ================== #
# ============================================================ #


# Path to directory containing the LARVIS executable script .
readonly LARVISPath=$(dirname $(readlink -f "$0"))


# Path to directory containing the LARVIS library (scripts) .
readonly LARVISLibPath="$LARVISPath/lib"


# Version Number
readonly LARVISVersion=1
readonly LARVISReVersion=0







# ============================================================ #
# ================== < Library Includes > ==================== #
# ============================================================ #


source "$LARVISLibPath/ColorUtils.sh"
source "$LARVISLibPath/HelpUtils.sh"
source "$LARVISLibPath/InstallUtils.sh"






################################# < Configrable Variable > ################################




ID=$EUID
P=$(pwd > ./RP)
W=$('whoami' >> ./RP)
HM=$('./HM.py')
if [ "$ID" = 0 ] ; then

     readonly LARVISPromptDefault="$CRed[${CSBlu}larvis${CDCyn}@${CSYel}root${CClr}${CRed}]-[$CCyn$HM$CClr$CRed]$CClr"
    LarvisPrompt=$LARVISPromptDefault

else

   readonly LARVISPromptDefault="$CRed[${CSBlu}larvis${CDCyn}@$CSGrn$HOSTNAME$CClr$CRed]-[$CCyn$HM$CClr$CRed]$CClr"
    LarvisPrompt=$LARVISPromptDefault

fi
readonly LARVISLineDefault="$CRed[$CSYel*$CClr$CRed]$CClr"
LARVISLine=$LARVISLineDefault












# ============================================================ #
# ================== < Load Configurables > ================== #
# ============================================================ #




# ============= < Argument Loaded Configurables > ============ #




while [ "$1" != "" ] && [ "$1" != "--" ]; do
  case "$1" in
    -v|--version) echo "LARVIS Version ---> $LARVISVersion.$LARVISReVersion"; exit;;
    -h|--help) larvis_help; exit;;
    -i|--install) readonly LARVISInstall=0; shift;;
    -d|--debug) readonly LARVISDebug=1;;
    -k|--killer) readonly LARVISWIKillProcesses=1;;
    -5|--5ghz) LARVISEnable5GHZ=1;;
    -r|--reloader) readonly LARVISWIReloadDriver=1;;
    -n|--airmon-ng) readonly LARVISAirmonNG=1;;
    -m|--multiplexer) readonly LARVISTMux=1;;
    -b|--bssid) LarvisTargetMAC=$2; shift;;
    -e|--essid) LarvisTargetSSID=$2;
      # TODO: Rearrange declarations to have routines available for use here.
      LarvisTargetSSIDClean=$(echo "$FluxionTargetSSID" | sed -r 's/( |\/|\.|\~|\\)+/_/g'); shift;;
    -c|--channel) LarvisTargetChannel=$2; shift;;
    -l|--language) LarvisLanguage=$2; shift;;
    -a|--attack) LarvisAttack=$2; shift;;
    -i|--install) LARVISSkipDependencies=0; shift;;
    --ratio) LARVISWindowRatio=$2; shift;;
    --auto) readonly LARVISAuto=1;;
    --skip-dependencies) readonly LARVISSkipDependencies=1;;
  esac
  shift # Shift new parameters
done

shift # Remove "--" to prepare for attacks to read parameters.
# Executable arguments are handled after subroutine definition.





# ============================================================ #
# =================== < Default Language > =================== #
# ============================================================ #
# Set by default in case fluxion is aborted before setting one.
source "$LARVISPath/language/en.sh"

