#!/usr/bin/env bash



LARVISInstall() {

    echo ""

    echo "                          Which operating system are you using ? "

    echo ""
    echo "      (1) Windows OS "
    echo "      (2) GNU/Linux based OS "
    echo "      (3) Mac OS "
    echo ""

    echo " Choose options with there number in whole process . Example :- 1 "
    echo ""

    read -p $PRO ANS

    if [ "$ANS" = 1 ] ; then

        echo ""
        echo " You have choosen Windows as your main operating system "
        echo ""

        echo ""
        echo " Windows OS "
        echo ""

    elif [ "$ANS" = 2 ] ; then

        echo ""
        echo " You have choosen GNU/Linux as your main operating system "
        echo ""

        echo ""
        echo "                      Which distribution are you running ? "
        echo ""

        echo "     (1) Debian "
        echo "     (2) Arch "
        echo "     (3) Gentoo "
        echo ""

        read -p $PRO ans

        echo ""

        if [ "$ans" = 1 ] ; then

            echo ""
            echo " You have choosen Debian as your current distrobution "
            echo ""

            if [ "$ID" = 0 ] ;  then

                echo " Updating the repository.... "

                apt-get update

                echo " Downloading required dependencies for the assistant to work fine ..."
                
                apt-get install python python3 python-pyaudio python3-pyaudio portaudio19-dev python-all-dev python3-all-dev flac python-pip build-essential python python3-pip swig git libpulse-dev python3 python3-all-dev pocketsphinx libasound-dev portaudio19-dev libportaudio2 libportaudiocpp0 libav-tools python3-pocketsphinx python3-sphinxbase pavucontrol jack2 jack2-dbus jackd2 jackd2-dbus jackd-studio jackd-stdio jack-stdio

                echo " Done..."

                echo " Now lets install some python dependencies ... "
                python -m pip install -r requirments.txt
                echo " Done..."

            else

                echo " Updating the repository.... "

                sudo apt-get update

                echo " Downloading required dependencies for the assistant to work fine ..."
                
                sudo apt-get install python python3 python-pyaudio python3-pyaudio portaudio19-dev python-all-dev python3-all-dev flac python-pip build-essential python python3-pip swig git libpulse-dev python3 python3-all-dev pocketsphinx libasound-dev portaudio19-dev libportaudio2 libportaudiocpp0 libav-tools python3-pocketsphinx python3-sphinxbase pavucontrol jack2 jack2-dbus jackd2 jackd2-dbus jackd-studio jackd-stdio jack-stdio

                echo " Done..."

                echo " Now lets install some python dependencies ... "
                python -m pip install -r requirments.txt
                echo " Done..."

            fi
            
        elif [ "$ans" = 2 ] ; then

            echo ""
            echo " You have choosen Arch as your current distrobution "
            echo ""

            if [ "$ID" = 0 ] ;  then

                echo " Updating the repository.... "

                pacman -Syu

                echo " Downloading required dependencies for the assistant to work fine ..."
                
                pacman -S python python3 python-pyaudio python3-pyaudio portaudio19-dev python-all-dev python3-all-dev flac python-pip build-essential python python3-pip swig git libpulse-dev python3 python3-all-dev pocketsphinx libasound-dev portaudio19-dev libportaudio2 libportaudiocpp0 libav-tools python3-pocketsphinx python3-sphinxbase pavucontrol jack2 jack2-dbus jackd2 jackd2-dbus jackd-studio jackd-stdio jack-stdio

                echo " Done..."

                echo " Now lets install some python dependencies ... "
                python -m pip install -r requirments.txt
                echo " Done..."

            else

                echo " Updating the repository.... "

                sudo pacman -Syu

                echo " Downloading required dependencies for the assistant to work fine ..."
                
                sudo pacman -S python python3 python-pyaudio python3-pyaudio portaudio19-dev python-all-dev python3-all-dev flac python-pip build-essential python python3-pip swig git libpulse-dev python3 python3-all-dev pocketsphinx libasound-dev portaudio19-dev libportaudio2 libportaudiocpp0 libav-tools python3-pocketsphinx python3-sphinxbase pavucontrol jack2 jack2-dbus jackd2 jackd2-dbus jackd-studio jackd-stdio jack-stdio

                echo " Done..."

                echo " Now lets install some python dependencies ... "
                python -m pip install -r requirments.txt
                echo " Done..."

            fi

        elif [ "$ans" = 3 ] ; then

            echo ""
            echo " You have choosen Gentoo as your current distrobution "
            echo ""

            if [ "$ID" = 0 ] ;  then

                echo " Updating the repository.... "

                emerge -uDN world

                echo " Downloading required dependencies for the assistant to work fine ..."
                
                emerge python python3 python-pyaudio python3-pyaudio portaudio19-dev python-all-dev python3-all-dev flac python-pip build-essential python python3-pip swig git libpulse-dev python3 python3-all-dev pocketsphinx libasound-dev portaudio19-dev libportaudio2 libportaudiocpp0 libav-tools python3-pocketsphinx python3-sphinxbase pavucontrol jack2 jack2-dbus jackd2 jackd2-dbus jackd-studio jackd-stdio jack-stdio

                echo " Done..."

                echo " Now lets install some python dependencies ... "
                python -m pip install -r requirments.txt
                echo " Done..."

            else

                echo " Updating the repository.... "

                sudo emerge -uDN world

                echo " Downloading required dependencies for the assistant to work fine ..."
                
                sudo emerge python python3 python-pyaudio python3-pyaudio portaudio19-dev python-all-dev python3-all-dev flac python-pip build-essential python python3-pip swig git libpulse-dev python3 python3-all-dev pocketsphinx libasound-dev portaudio19-dev libportaudio2 libportaudiocpp0 libav-tools python3-pocketsphinx python3-sphinxbase pavucontrol jack2 jack2-dbus jackd2 jackd2-dbus jackd-studio jackd-stdio jack-stdio

                echo " Done..."

                echo " Now lets install some python dependencies ... "
                python -m pip install -r requirments.txt
                echo " Done..."
            fi

        else 

            echo ""
            echo " Please enter a correct choice !!! "
            echo ""
            LARVISInstall

        fi

    elif [ "$ANS" = 3 ] ; then

        echo ""
        echo " You have choosen Mac as your main operating system "
        echo ""

        echo""
        echo " MAC "
        echo""

    else 

        echo ""
        echo " Please enter a correct choice !!! "
        echo ""
        LARVISInstall

    fi
    
}

################################# < Configrable Variable > ################################




ID=$EUID 
if [ "$ID" = 0 ] ; then
    PC=$(pwd > /tmp/P)
    P=`cat /tmp/P`
    WC=$('whoami' > /tmp/W)
    W=`cat /tmp/W`
    HMC=$('./HM.py' > /tmp/H)
    HM=`cat /tmp/H`
        LARVISPromptDefault="$CRed[${CSBlu}larvis${CDCyn}@${CSYel}root${CClr}${CRed}]-[$CCyn$HM$CClr$CRed]$CClr"
        LarvisPrompt=$LARVISPromptDefault

    LARVISLineDefault="$CRed[$CSYel*$CClr$CRed]$CClr"
    LARVISLine=$LARVISLineDefault

    ID=$EUID
    PRO=$(echo -e $LarvisPrompt)



                                        ####################################################
                                        #                                                  #
                                        #                Installation                      #
                                        #                                                  #
                                        ####################################################




if [ "$LARVISInstall" ]; then return 0; fi


LARVISInstall

    
else
    echo ""
    echo " Be root to perfom this action !!!"
    echo ""

fi

