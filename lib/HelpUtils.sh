#!/usr/bin/env bash


larvis_help(){
  echo""
  echo " LARVIS(1)                       Help Menu                        LARVIS(1)



  NAME
         larvis  -  Larvis  is  a  system  assistant  and  a  usr  helper  for  any
         linux  User  out  there

  SYNOPSIS
         larvis [ -option ]

  DESCRIPTION
         Larvis is a system assistant and a usr helper for any linux User out there
         Want any help ? Just ask it , and it will definetly solved by Larvis . It
         is made from scratch for you all by the developer Prateekshit Jaiswal with
         more functionality and less bugs . An extensible and hackable program for
         it's users. It's compatible with all GNU/Linux based operating systems out
         there . More functionalities are on the way , so don't miss to update it .

  OPTIONS
         -v, --version                      Print version number.

         -h, --help                         Display a help screen and quit.

         -i, --install                      Install larvis on the system

         -m     Run larvis in manual mode instead of auto mode.

         -k     Kill wireless connection if it is connected.

         -d     Run larvis in debug mode.

         -x     Try to run larvis with xterm terminals instead of tmux.

         -r     Reload driver.

         -l <language>
                Define a certain language.

         --ratio <ratio>
                Define the windows size. Bigger ratio ->  smaller  window  size.
                Default is 4.


  FILES
         /tmp/larvspace/
                The system wide tmp directory.

  ENVIRONMENT

         LARVISDebug
                Automatically run larvis in debug mode if exported.

         LARVISWIKillProcesses
                Automatically kill any interfering process(es).

  DIAGNOSTICS
         Please checkout the other log files or use the debug mode.

  BUGS
         Please  report  any  bugs  at:  https://gitlab.com/prateekshitjaiswal73/larvis/issues

  AUTHOR
         Prateekshit Jaiswal



  Linux                             MARCH 2021                        LARVIS(1)"

}


