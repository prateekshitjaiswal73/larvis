import os

import datetime

import speech_recognition as sr

import webbrowser

import mysql.connector as sql

import wikipedia

import random

import time

db = sql.connect(host='localhost', user='root', passwd='12345')

con = db.cursor()

# query = ''

s = True


def wish():

    hour = int(datetime.datetime.now().hour)

    if 0 >= hour > 12:

        os.system("festival --tts ./texts/GM")

    elif 12 <= hour < 18:

        os.system("festival --tts ./texts/GA")

    else:

        os.system("festival --tts ./texts/GE")


def command():

    global query

    r = sr.Recognizer()

    mic = sr.Microphone(device_index=5)

    with mic as source:

        print("Listening...")

        r.adjust_for_ambient_noise(source)  # , duration=int(1))

        r.energy_threshold = 100

        r.pause_threshold = 0.5

        audio = r.listen(source)

        print("Wait a second .")

        query = ''

        try:

            print("Recognizing...")

            query = r.recognize_google(audio, language="en-in")

            print("user Said :- ", query)

            return query

        except Exception as e:

            print(e)

            os.system("festival --tts ./texts/SA")

            return "None"

    # except sr.RequestError as e:

    #   print("Sphinx error; {0}".format(e))


if __name__ == "__main__":

    wish()

    while True:

        q = command()

        query = q.lower()

        # WIKIPEDIA

        if "wikipedia" in query.lower():

            os.system("festival --tts ./texts/SP")

            query = query.replace("wikipedia", "")

            results = wikipedia.summary(query, sentences=3)

            # os.system("festival --tts ")

            with open("./texts/R", "w") as f:

                f.write(results)

                f.flush()

            print(results)

            os.system("festival --tts ./texts/R")

            print()

        # YOUTUBE

        elif "surf youtube" in query:

            webbrowser.open('youtube.com')

            print()

        # KALI

        elif "surf kali" in query.lower():

            webbrowser.open("kali.org")

            print()

        # MUSIC

        elif "play music" in query:

            os.system("parole")

            print()

        # TIME

        elif "time" in query:

            d = datetime.datetime.now()

            with open("./texts/D", "w") as f:

                f.write(str(d))

                f.flush()

            print(d)

            os.system("festival --tts ./texts/D")

            print()

        # HELP FROM STACKOVERFLOW

        elif "open stackoverflow" in query:

            webbrowser.open("stackoverflow.com")

            print()

        # MAIL

        elif "open mail" in query:

            os.system("thunderbird")

            print()

        # browser.open_new("gmail.com")

        # NEED A RANDOM NUMBER

        elif "pick a random number" in query:

            w = random.randint(0, 6)

            with open("./texts/RE", "w") as f:

                f.write(str(w))

            print(w)

            os.system("festival --tts ./texts/RE")

            print()

        # SURF MYSQL

        elif "search from database" in query:

            os.system("festival --tts ./texts/DS")

            database = command().lower()

            if con.execute("use {}".format(database)):

                s = True

            elif not s:

                os.system("festival --tts ./texts/DNF")

            os.system("festival --tts ./texts/STN")

            table = command().lower()

            if con.execute("select * from {}".format(table)):

                s = True

            elif not s:

                os.system("festival --tts ./texts/TNF")

            re = con.fetchall()

            for c in re:

                print(c)

            print()

        # RELAX

        elif "wait" in query:

            time.sleep(10)

            os.system("festival --tts ./texts/T")

            print()

        # OPEN ANY APP

        elif "open " in query:

            os.system("festival --tts ./texts/WAS")

            app = command().lower()

            os.system("app")

            print()

        # Opening python

        elif "start python" in query:

            os.system("festival --tts ./texts/OI")

            os.system("idle")

            print()

        # Vim

        elif "start vim" in query:

            os.system("festival --tts ./texts/AV")

            os.system("alacritty -e vim")

            print()

        # Talking to larvis

        elif "hi larvis" in query:

            os.system("festival --tts ./texts/H")

            print()

        # Responding to last question asked above

        elif "i am fine" in query:

            os.system("festival --tts ./texts/N")

            print()

        # Update Qtile

        elif "update system" in query:

            os.system("festival --tts ./texts/J")

            os.system("bash ~/update_qtile.sh")

        # Exit

        elif "exit" in query:

            os.system("festival --tts ./texts/TS")

            print()

            exit()
